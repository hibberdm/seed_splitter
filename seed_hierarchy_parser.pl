#!/usr/bin/perl -w

#perl seed_hierarchy_parser.pl -i NC_009614.normalized -s BVU_seed_hierarchy_table.tsv -o compare_hierarchy.txt > output_hierarchy.txt
#perl seed_hierarchy_parser.pl -i NC_009614.normalized -s bindings.txt -o compare_bindings.txt -b > output_bindings.txt

use strict;
use Getopt::Long;
use lib "./";

my $input_file;
my $output_file;
my $seed_file;
my $normalized;
my $bindings_parse;

GetOptions ( 
            "infile=s"   => \$input_file,
			"seed=s"   => \$seed_file,
            "outfile=s"   => \$output_file,
            "bindings" => \$bindings_parse,
            );  # flag

usage() unless $seed_file;

unless ($output_file) {
	$output_file = "default_output.txt";
	}

open(IN, "<$input_file") or die "can't open $input_file: $!\n";
open(DIAG, "+>diagnostics.txt") or die "can't open diagnostics.txt: $!\n";

use LOCUS_TO_PEG;
my %locus_to_peg = locus_lookup;
my $hier_ref = "";
my $levels_count = "";
if ($bindings_parse) {
	$hier_ref = parse_seed_bindings($seed_file);
	$levels_count = 2;
	}
else {
	$hier_ref = parse_seed_hierarchy($seed_file);
	$levels_count = 4;
	}

my %seed_hierarchy = %$hier_ref;
my %rnaseq_data = ();
my @headers = ();
my $line_counter = 0;

#Read input RNA-Seq file into memory
while (my $line = <IN>) {
	chomp $line;
	if ($line_counter == 0) {
		@headers = split(/\t/, $line);
		}
	else {
		my @array = split(/\t/, $line);
		foreach my $index (0 .. $#array) {
			if ($headers[$index] =~ /^status:(.+)/) {
				push (@{$rnaseq_data{$1}{"status"}}, $array[$index]);
				}
			elsif ($headers[$index] =~ /^counts:(.+)/) {
				push (@{$rnaseq_data{$1}{"counts"}}, $array[$index]);
				}
			elsif ($headers[$index] =~ /^scale:(.+)/) {
				push (@{$rnaseq_data{$1}{"scale"}}, $array[$index]);
				}
			elsif ($headers[$index] =~ /^size_scale:(.+)/) {
				push (@{$rnaseq_data{$1}{"size_scale"}}, $array[$index]);
				}
			elsif ($headers[$index] =~ /^gene/) {
				push (@{$rnaseq_data{"gene"}{"gene"}}, $array[$index]);
				}
			else {
				die "ERROR: Unrecognized header found --> $array[$index]\n";
				}
			}
		}
	$line_counter++;
	}

#
my @samples = ();
my @sort_array = sort keys %rnaseq_data;
foreach my $key (@sort_array) {
	unless ($key eq "gene") {
		push @samples, $key;
		}
	}

#print DIAG join("\n", @samples);

#prepare the seed hierarchy to log data based on sample number
foreach my $peg (keys %seed_hierarchy) {	
	for (my $i = 0; $i < scalar(@samples); $i++)	{
		push(@{$seed_hierarchy{$peg}{raw_counts}},0);
		push(@{$seed_hierarchy{$peg}{rpkm_counts}},0);
			}		
		}

print_counted_hierarchy(\%seed_hierarchy, $seed_file);
my %found_hash = ();
my %missing_hash = ();

for (my $i = 0; $i < scalar(@samples); $i++)	{
	my $sample = $samples[$i];
	print DIAG "Sample $i ($sample)";
	for (my $j = 0; $j < scalar(@{$rnaseq_data{"gene"}{"gene"}}); $j++)	{
		my $gene = @{$rnaseq_data{"gene"}{"gene"}}[$j];
		print DIAG "\tGene $j ($gene)\n";
		if (exists $locus_to_peg{$gene} && exists $seed_hierarchy{$locus_to_peg{$gene}}) {
#			print "Found locus: $gene => PEG: $locus_to_peg{$gene}\n";
			$found_hash{$gene} = $locus_to_peg{$gene};
			@{$seed_hierarchy{$locus_to_peg{$gene}}{raw_counts}}[$i] += @{$rnaseq_data{$sample}{"counts"}}[$j];
			@{$seed_hierarchy{$locus_to_peg{$gene}}{rpkm_counts}}[$i] += @{$rnaseq_data{$sample}{"size_scale"}}[$j];
			}
		else {
			print DIAG "Locus $gene not found, unable to enter SEED hierarchy (adjust script possible)!\n";
			$missing_hash{$gene} = "not_found";
			}
		}
	}

my $found = scalar(keys %found_hash);
my $missing = scalar(keys %missing_hash);
my $total = $found + $missing;
my $rnaseq_gene_total = scalar(@{$rnaseq_data{"gene"}{"gene"}});
my $found_fraction = $found/($total);
my $missing_fraction = $missing/($total);
my $parsed_seed_pegs = scalar(keys %seed_hierarchy);
my @missing_from_seed = sort keys %missing_hash;
print "PEGs in parsed SEED file: $parsed_seed_pegs\n";
print "Total sequenced genes (from $input_file): $rnaseq_gene_total\n";
print "Fraction of sequenced genes found in SEED hierarchy: $found_fraction ($found/$total)\n";
print "Fraction of sequenced genes NOT found in SEED hierarchy: $missing_fraction ($missing/$total)\n";
print DIAG "\nFeatures not found:\n\t".join("\t", @missing_from_seed)."\n";

print_counted_hierarchy(\%seed_hierarchy, $seed_file);

output_summarized_hierarchy(\%seed_hierarchy, $output_file, \@samples, $levels_count);

close IN;

sub parse_seed_hierarchy {
	my $handle = shift;
	open(SEED, "<$handle") or die "can't open $handle: $!\n";
	my %hierarchy = ();
	while (my $line = <SEED>) {
		chomp $line;
		my @levels = split('\t', $line);
		unless ($levels[0] eq "Category") {
			my @pegs = split(", ", $levels[4]);		#note that this also includes rnas, not just "pegs."
			for (my $i = 0; $i < scalar(@pegs); $i++) {
				my $peg_number = "none";
				if ($pegs[$i] =~ /.*\.(\d+)$/) {
					$peg_number = $1;
					}
				else {
					die "unable to parse peg number for sorting\n";
					}
#				print "$peg_number\n";
				my @entry = ($levels[0], $levels[1], $levels[2], $levels[3]);
							 #category   #subcat     #subsys     #role
				push @{$hierarchy{$pegs[$i]}{levels}}, \@entry;
				$hierarchy{$pegs[$i]}{peg_number} = $peg_number;
#				print "$hierarchy{$pegs[$i]}{\"number\"}\n";
				}
			}
		}	
	close SEED;
	print_parsed_hierarchy(\%hierarchy, $handle);
	return \%hierarchy;
	}

sub parse_seed_bindings {
	my $handle = shift;
	open(SEED, "<$handle") or die "can't open $handle: $!\n";
	my %hierarchy = ();
	while (my $line = <SEED>) {
		chomp $line;
		my @levels = split('\t', $line);
		my $peg_number = "none";
		if ($levels[2] =~ /.*\.(\d+)$/) {
			$peg_number = $1;
			}
		else {
			die "unable to parse peg number for sorting\n";
			}
#			print "$peg_number\n";
			my @entry = ($levels[0], $levels[1]);
						#subsystem   #function
			push @{$hierarchy{$levels[2]}{levels}}, \@entry;
			$hierarchy{$levels[2]}{peg_number} = $peg_number;
#				print "$hierarchy{$pegs[$i]}{\"number\"}\n";
		}
	close SEED;
	print_parsed_hierarchy(\%hierarchy, $handle);
	return \%hierarchy;
	}
	
sub print_parsed_hierarchy	{
	my $ref = shift;
	my $parsed_handle = shift;
	my %parsed_hierarchy = %$ref;
	open (PARSED, "+>$parsed_handle".".parsed") or die "can't open parsed seed output file: $!\n";	
	foreach my $peg (sort { $parsed_hierarchy{$a}{peg_number} <=> $parsed_hierarchy{$b}{peg_number} } keys %parsed_hierarchy) {	
		print PARSED "$peg\n";
		for (my $i = 0; $i < scalar(@{$parsed_hierarchy{$peg}{levels}}); $i++) { 
			my $test = join("\t", @{$parsed_hierarchy{$peg}{levels}[$i]});
			print PARSED "\t$test\n";
			}
		}
	close PARSED;
	}

sub print_counted_hierarchy	{
	my $ref = shift;
	my $parsed_handle = shift;
	my %parsed_hierarchy = %$ref;
	open (COUNTED, "+>$parsed_handle".".parsed.counted") or die "can't open parsed seed output file: $!\n";	
	foreach my $peg (sort { $parsed_hierarchy{$a}{peg_number} <=> $parsed_hierarchy{$b}{peg_number} } keys %parsed_hierarchy) {	
		print COUNTED "$peg\n";
		for (my $i = 0; $i < scalar(@{$parsed_hierarchy{$peg}{levels}}); $i++) { 
			my $test = join("\t", @{$parsed_hierarchy{$peg}{levels}[$i]});
			print COUNTED "\t$test\n";
			}
		my $test2 = join("\t", @{$parsed_hierarchy{$peg}{raw_counts}});
		my $test3 = join("\t", @{$parsed_hierarchy{$peg}{rpkm_counts}});
		print COUNTED "\t$test2\n\t$test3\n";
		}
	close COUNTED;
	}

#trouble - subsystems rarely repeat within a peg, but functions or higher-res levels do
#I chose to ignore repeated levels within a given peg.
sub output_summarized_hierarchy	{
	my $hashref = shift;
	my $handle = shift;
	my $arrayref = shift;
	my $levels = shift;
	my %counted_hierarchy = %$hashref;
	my @samples = @$arrayref;
	my $header = "SEED\t".join("\t", @samples);
	for (my $i = 0; $i < $levels; $i++) {
		my %level_summary = ();
		open (RAW, "+>$handle".".SL".$i.".counts.txt") or die "can't open output file: $!\n";	
		open (RPKM, "+>$handle".".SL".$i.".rpkm.txt") or die "can't open output file: $!\n";	
		print RAW "$header\n";
		print RPKM "$header\n";	
		foreach my $peg (keys %counted_hierarchy)	{
#			print RAW "$peg\n";
			my %seen_hash = ();
			for (my $j = 0; $j < scalar(@{$counted_hierarchy{$peg}{levels}}); $j++) {
				print "\t$peg\t$counted_hierarchy{$peg}{levels}[$j][$i]\n";
				if (exists $seen_hash{$counted_hierarchy{$peg}{levels}[$j][$i]}) {
					print "Double skipped\n";
					}
				else {
#					if (exists $level_summary{$counted_hierarchy{$peg}{levels}[$j][$i]}{raw_counts}) {
#						for (my $k = 0; $k < scalar(@{$counted_hierarchy{$peg}{raw_counts}}); $k++) {
#							$level_summary{$counted_hierarchy{$peg}{levels}[$j][$i]}{raw_counts}[$i][$k] += $counted_hierarchy{$peg}{raw_counts}[$k];
#							$level_summary{$counted_hierarchy{$peg}{levels}[$j][$i]}{rpkm_counts}[$i][$k] += $counted_hierarchy{$peg}{rpkm_counts}[$k];
#							}
#						}
#					else {
#						$level_summary{$counted_hierarchy{$peg}{levels}[$j][$i]}{raw_counts}[$i] = \@{$counted_hierarchy{$peg}{raw_counts}};
#						$level_summary{$counted_hierarchy{$peg}{levels}[$j][$i]}{rpkm_counts}[$i] = \@{$counted_hierarchy{$peg}{rpkm_counts}};
#						}					
					for (my $k = 0; $k < scalar(@{$counted_hierarchy{$peg}{raw_counts}}); $k++) {
						$level_summary{$counted_hierarchy{$peg}{levels}[$j][$i]}{raw_counts}[$i][$k] += $counted_hierarchy{$peg}{raw_counts}[$k];
						$level_summary{$counted_hierarchy{$peg}{levels}[$j][$i]}{rpkm_counts}[$i][$k] += $counted_hierarchy{$peg}{rpkm_counts}[$k];
						}
					}
				$seen_hash{$counted_hierarchy{$peg}{levels}[$j][$i]} = 1;
				}
			}
			foreach my $key (sort keys %level_summary) {
#				print "Level $i => Name $key\n";
				print RAW $key."\t".join("\t", @{$level_summary{$key}{raw_counts}[$i]})."\n";
				print RPKM $key."\t".join("\t", @{$level_summary{$key}{rpkm_counts}[$i]})."\n";
				}
			close RAW;		
			close RPKM;
		}
	}

sub usage {
	print "\n\tUsage: perl seed_hierarchy_parser.pl -s <seed_hierarchy or bindings file [REQUIRED]>\n".
	"\t\t-i <input .normalized file from RNA-Seq analysis pipeline>\n" .
	"\t\t-o <output file basename>\n".
	"\t\t-b <FLAG, use if using SEED bindings.txt rather than SEED hierarchy tsv file>\n".
	"\tExamples:\n".
	"\t\tperl seed_hierarchy_parser.pl -i NC_009614.normalized -s BVU_seed_hierarchy_table.tsv -o compare_hierarchy.txt > output_hierarchy.txt\n".
	"\t\tperl seed_hierarchy_parser.pl -i NC_009614.normalized -s bindings.txt -o compare_bindings.txt -b > output_bindings.txt\n".
	"\n";
	exit(1);
	}